import java.util.Arrays;

public class Game {
    private String[]  moves;
    private enum Type{
        RPSLS,
        RPS
    }
    private Type gameType;

    public Game(String[] args) throws IncorrectMovesException {
        Arrays.sort(args);

        if(args.length == 3){
            gameType = Type.RPS;
            String[] moves = new String[]{"paper", "rock", "scissors"};
            if (!Arrays.equals(args, moves))
                throw new IncorrectMovesException();
            else
                this.moves = moves;
        }
        else if(args.length == 5) {
            gameType = Type.RPSLS;
            if (!Arrays.equals(args, new String[]{"lizard", "paper", "rock", "scissors", "spock"}))
                throw new IncorrectMovesException();
            else
                moves = new String[]{"scissors", "paper", "rock", "lizard", "spock"};
        }
    }

    private boolean checkMove(int move){
        return move >= 0 && move < moves.length;
    }
    public void play(){
        int userMove;
        int computerMove;
        User user = new User();
        Enemy enemy = new Enemy();
        while (true) {
            switch (gameType){
                case RPS:
                    System.out.println(StringUtils.MENU_FOR_3_ELEMENTS);
                    break;
                case RPSLS:
                    System.out.println(StringUtils.MENU_FOR_5_ELEMENTS);
                    break;
            }
            System.out.print("Your choice: ");
            userMove = user.getMove() - 1;
            if (userMove == -1)
                return;
            computerMove = enemy.getComputerMove(moves.length);

            if (!(checkMove(userMove) && checkMove(computerMove))) {
                System.out.println("Invalid input! Please, make correct choice");
                continue;
            }
            System.out.printf("Your move: %s\n", moves[userMove]);
            System.out.printf("Enemy move: %s\n", moves[computerMove]);

            Boolean gameResult = fight(userMove, computerMove);
            if(gameResult == null)
                System.out.println("Draw!");
            else if(gameResult)
                System.out.println("Victory!");
            else
                System.out.println("Defeat!");
        }
    }

    private Boolean fight(int userMove, int enemyMove) {
        int difference = userMove - enemyMove;
        System.out.printf("Difference: %s\n", difference);

        if(difference == 0)
            return null;
        else if (difference == -1 || difference == moves.length - 1 || (difference%2==0 && difference>0) || (difference == moves.length - 2 && difference < 0))
            return true;
        else
            return false;
    }
}
