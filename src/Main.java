import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try {
            Game game = new Game(args);
            game.play();
        } catch (IncorrectMovesException e) {
            e.printStackTrace();
        }
//        byte[] key = HMAC.generateKey();
//        System.out.println(key.toString());
//        String hmac = Arrays.toString(HMAC.calcHmacSha256(key, "spoke"));
//        System.out.println(hmac);
    }
}
