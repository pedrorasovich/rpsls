public class IncorrectMovesException extends Exception {
    public IncorrectMovesException() {
        super("Invalid moves!");
    }
}
